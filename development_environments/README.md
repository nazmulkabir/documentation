# Development Environments
## Table of Contents
- [Quickstart](#quickstart)
- [Overview](#overview)
- [Usage](#usage)
- [Tutorials](#tutorials)
- [Freqently Asked Questions](#frequently-asked-questions)
- [Key Terms](#key-terms)

## Quickstart
1. In your web browser, navigate to https://jupyterhub-odh-test.apps.ncr1.aitb.cyberinitiative.org.
2. Specify your desired resources and click "start" (e.g., `notebook image`, `container` size, number of GPUs)
3. Start your research!


## Overview
The CCI AI Testbed provides users with access to interactive python development environments using `jupyter notebooks`.  The `jupyter notebook` web browser-based environment is made available without any configuration required by the end user (e.g., operating system installation, security configuration, package maintenance, etc.).  

Get up and running in three simple steps:
1. Login to the testbed
2. Specify your resource requirements (e.g., GPU quantity, CPU quantity, RAM, storage)
3. Create a `jupyter notebook`

![jupyter lab](jupyter_lab.png)

## Usage
A user interfaces with `jupyter notebooks` through their web browser.  The `jupyter notebook` runs inside a `container` on the testbed.  The hardware resources dedicated to the `container` (and consequently, the `jupyter notebook`) are specified by the user prior to starting the notebook.

When the user's `container` starts, a `persistent volume` is dynamically provisioned and mounted to the container.  When the users shuts down their `jupyter notebook`, this `persistent volume` will remain persistent.  When the user starts their notebook again, the `persistent volume` containing their files/data will be remounted to the new `container`.

## Tutorials
- [I want to create a jupyter notebook.](tutorials/creating_a_notebook)
- [I want to use GPUs while running code in my jupyter notebook.](tutorials/using_gpus)

## Frequently Asked Questions
1. **Will my files persist after I shutdown my `jupyter notebook`?** \
Yes, when your `container` starts, a `persistent volume` is dynamically provisioned and mounted to the container.  When you shutdown your `jupyter notebook`, this `persistent volume` will remain persistent.  Simply start your `jupyter notebook` again and this `persistent volume` will be re-mounted to your container for easy access.
1. **I don't have enough space in my `container` to store my dataset.** \
The `persistent volume` mounted to the `container` provides users with persistent storage for small files (e.g., code, small datasets).  The CCI AI Testbed provides users with `S3 object storage` for datasets that will not fit into the `persistent volume`.  Documentation on the `S3 object storage` is available [here](../storage).

## Key Terms
- `container`:
- `jupyter notebook`:
- `persistent volume`:
- `notebook image`:
- `S3 object storage`:
