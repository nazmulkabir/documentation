# Creating a Notebook
1.  Specify the `notebook image` you would like:
![specifying image](specifying_image.png)

1. Specify the `container` size you would like:
![specifying container size](specifying_container_size.png)

1. (Optional) If you have environment variables to specify click "Add more variables". \
  **Note: specifying environment variables (e.g., `AWS ACCESS_KEY_ID`) on `container`/`jupyter notebook` startup is a good best practice to avoid sensitive credentials from mistakenly being committed to version control (e.g., git).**
![adding environment variable](adding_env_var.png)

1. (Optional) Specify the environment variables you would like to set: \
  **Note: You may optionally select the check box to prevent the secret from appearing on your screen.**
![specifying environment variable](adding_env_secret.png)

1. Select "Start"
![start notebook](start_notebook.png)

1. The notebook will start up:
![notebook starting up](notebook_starting_up.png)

1. Once the notebook has started, you will be automatically redirected to the default Jupyter Notebook view.  
![notebook_jupyter_view](jupyter_notebook_default.png)

1. Note:  If you prefer, you may use the `Jupyter Lab` interface, by navigating to the following link, where `YOUR_USERNAME` is replaced by the username you used to login to the testbed:
```link
https://jupyterhub-odh-test.apps.ncr1.aitb.cyberinitiative.org/user/<YOUR_USERNAME>/lab
```
