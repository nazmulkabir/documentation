# Interacting with Object Storage via `S3` API using the AWS CLI

## Prerequisites
1. The `aws` CLI must be installed.  For directions on installation, please follow the tutorial [here](../install_aws_cli/README.md).
1. A `bucket` must exist within your `project`.  For a directions on creating `buckets`, please following the tutorial [here](../create_bucket/README.md)
1. Configure `aws` CLI with your `bucket` credentials.
##### Windows
```bash
aws configure
```
##### Linux
```bash
aws configure
```
1. For easy reference later, please set a command line environment variable with the `bucket` name.
##### Windows
```bash
set BUCKET_NAME=<ENTER BUCKET NAME HERE>
```
##### Linux
```bash
export BUCKET_NAME=<ENTER BUCKET NAME HERE>
```

## Process

### List Buckets
1. Enter the following command
##### Windows
```bash
aws s3 ls --endpoint="https://s3-openshift-storage.apps.ncr1.aitb.cyberinitiative.org" --no-verify
```
##### Linux
```bash
aws s3 ls --endpoint="https://s3-openshift-storage.apps.ncr1.aitb.cyberinitiative.org" --no-verify
```

### List Objects in Bucket
1. Enter the following command
##### Windows
```bash
aws s3 ls s3://%BUCKET_NAME% --endpoint="https://s3-openshift-storage.apps.ncr1.aitb.cyberinitiative.org" --no-verify
```
##### Linux
```bash
aws s3 ls s3://$BUCKET_NAME --endpoint="https://s3-openshift-storage.apps.ncr1.aitb.cyberinitiative.org" --no-verify
```

### Upload file to bucket
1. Navigate to the directory of the file you would like to upload
##### Windows
```bash
cd C:\path\to\directory
```
##### Linux
```bash
cd /path/to/directory
```

2. Enter the following command, replacing your file name below:
##### Windows
```bash
aws s3 cp <ENTER FILE NAME HERE> s3://%BUCKET_NAME% --endpoint="https://s3-openshift-storage.apps.ncr1.aitb.cyberinitiative.org" --no-verify
```
##### Linux
```bash
aws s3 cp <ENTER FILE NAME HERE> s3://$BUCKET_NAME --endpoint="https://s3-openshift-storage.apps.ncr1.aitb.cyberinitiative.org" --no-verify
```
