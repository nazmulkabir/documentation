# Installing `aws` CLI
## Prerequisites
None

## Procedure
1. Navigate and follow the `aws` command line interface (CLI) installation directions located [here](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html).
