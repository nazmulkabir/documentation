# Creating Object Storage Buckets

## Prerequisites
1. User must have an account on the CCI AI CCI AI Testbed

## Procedure
1. Navigate to the CCI AI Testbed portal located [here](https://console-openshift-console.apps.ncr1.aitb.cyberinitiative.org/)
1. Select "Developer" in the top left-hand corner of your screen and select "Administrator" from the list that appears:
![alt text](cci_ai_testbed_portal.png "CCI AI Testbed Portal")
1. Select "Storage" from the navigation bar on the left:
![alt text](administration_view.png "Select Storage")
1. Select "Object Bucket Claims" from the navigation bar on the left:
![alt text](select_object_bucket_claim.png "Select Object Bucket Claim")
1. Select your project from the "Project" dropdown at the top of your screen.
![alt text](select_project.png "Select Project")
1. Select "Create Object Bucket Claim" on the button on the left.
![alt text](create_object_bucket_claim.png "Create Object Bucket Claim")
1. Enter the name of the bucket you would like to create (e.g., 'super-cool-dataset').  Note: the name must be 1) all lower-case letters and 2) contain no spaces.
![alt text](name_object_bucket_claim.png "Name Object Bucket Claim")
1. From the "Storage Class" drop-down list, select "openshift-storage.noobaa.io":
![alt text](select_storage_class.png "Select Storage Class")
1. Select "Create"
![alt text](create_bucket.png "Create Bucket")
