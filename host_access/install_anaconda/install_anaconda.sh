#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color


echo "Downloading Anaconda, a suite of python packages and virtual environment for data science."
wget -O ~/Downloads/Anaconda3-2020.07-Linux-x86_64.sh https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh
HASHED=$(md5sum ~/Downloads/Anaconda3-2020.07-Linux-x86_64.sh)
DIFF=$(diff <(echo "$HASHED") <(echo '1046c40a314ab2531e4c099741530ada'))
if [ "$DIFF" != "" ]; then
  echo "Hashes matched"
else
  printf "${RED}Hashes didn't match.  Could not install Anaconda.\n"
  exit 1
fi


echo "Installing Anaconda"
chmod 700 ~/Downloads/Anaconda3-2020.07-Linux-x86_64.sh
bash ~/Downloads/Anaconda3-2020.07-Linux-x86_64.sh -b
if grep -q "conda" ~/.bashrc; then
  echo "Already added anaconda to bashrc"
else
  echo "Adding anaconda to bashrc"
  echo ". /home/$USER/anaconda3/etc/profile.d/conda.sh" >> ~/.bashrc
  echo "conda activate base" >> ~/.bashrc
fi