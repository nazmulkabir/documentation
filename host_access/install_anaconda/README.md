# Installing Anaconda
Users do not have access to install python packages on the host system.  Therefore,
we must install our python packages into our user space.

Python packages are easiest to manage using a package manager such as pip or Anaconda.

## Clone this repository
Clone this repository to an easy to find/use location by running:
```
# Cloning the repository
git clone git@code.vt.edu:cci-ai-assurance-testbed/documentation.git

# Setting the project directory environment variable for later use
cd documentation
export PROJECT_DIR=$(pwd)
```

## Run the `setup.sh` script to install Anaconda

Run the `setup_anaconda.sh` file provided by executing the following in your shell:
```
# Changing shell script to be executable
chmod 700 $PROJECT_DIR/host_access/install_anaconda/install_anaconda.sh

# Running the install script
source $PROJECT_DIR/host_access/install_anaconda/install_anaconda.sh

# Activate conda for the first time
source ~/.bashrc
```
Congratulations!  You now have anaconda installed.  Since your `~/.bashrc` file contains the conda activation script, every time you login to the CCI AI Assurance Testbed, your base anaconda environment will be activated.
