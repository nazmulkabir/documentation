## Using the Host
With our recent releases, Jupyter Notebooks are now provided out of the box and require no manual configuration/deployment by testbed users.  Documentation for this capability is provided [here](../../development_environments).

Please contact AI Testbed Support if this is a capability you would like to gain access to.

**Note: Because our new release provides this functionality out of the box, host-based jupyter notebooks are no longer supported.**
